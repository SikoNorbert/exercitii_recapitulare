package car;

public class Executie {
    public static void main(String[] args) {
//        Car x1 = new Car(); //Am construit o masina noua
//        System.out.println(x1.getNumeMasina()); // null
//        System.out.println(x1.getCuloare());    // null
//
//        Car x2 = new Car(); //Am construit o masina noua
//        Car x3 = new Car("Buick"); // are un parametru
//        Car x4 = new Car("Ford", "rosie"); //are 2 parametrii
//        Car x5 = new Car("galben", 1980);
//        Car x6 = new Car(1981, "galben");
//
//        System.out.println(x2.getNumeMasina()); // null
//        System.out.println(x2.getCuloare());    // null
//
//        System.out.println(x3.getNumeMasina()); // Buick
//        System.out.println(x3.getCuloare());    // null
//        System.out.println(x3.getAnFabricatie()); // 0
//

        Car x9 = new Car();
        System.out.println(x9.getNumeMasina());
        x9.setNumeMasina("mercedes");
        System.out.println(x9.getNumeMasina());

    }
}
