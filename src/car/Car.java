package car;

public class Car {

    private String numeMasina;
    private String culoare;
    private int anFabricatie;

    //constructorii ii pot implementa daca
    //1. au nr parametrii diferiti
    //2. au tipul de parametrii diferiti
    //3. au ordinea parametrilor diferiti

    //constructor implicit
    public Car() {
        System.out.println("Am construit o masina");

    }

    //constructori expliciti
    public Car(String numeMasina) {
        this.numeMasina = numeMasina;
    }

    public Car(String numeMasina, int anulFabricatieiMasinii) {
        this.numeMasina = numeMasina;
        this.anFabricatie = anulFabricatieiMasinii;
    }

    public Car(int anulFabricatieiMasinii, String culoare) {
        this.anFabricatie = anulFabricatieiMasinii;
        this.culoare = culoare;
    }

    public Car(String numeleMasiniiEste, String culoareaMasiniiEste) {
        this.numeMasina = numeleMasiniiEste;
        this.culoare = culoareaMasiniiEste;
    }


    public String getNumeMasina() {
        return numeMasina;
    }

    public void setNumeMasina(String parametru) {
        this.numeMasina = parametru;
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public int getAnFabricatie() {
        return anFabricatie;
    }

    public void setAnFabricatie(int anFabricatie) {
        this.anFabricatie = anFabricatie;
    }
}
