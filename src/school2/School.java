package school2;

import java.util.Arrays;
import java.util.Scanner;

public class School {
    private String name;
    private String address;
    private int numberOfStudents;
    private int numberOfRooms;
    private String schoolColor;
    private int numberOfTeachers;
    private String[][] orar = new String[7][1];
    private String[] courses;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getSchoolColor() {
        return schoolColor;
    }

    public void setSchoolColor(String schoolColor) {
        this.schoolColor = schoolColor;
    }

    public int getNumberOfTeachers() {
        return numberOfTeachers;
    }

    public void setNumberOfTeachers(int numberOfTeachers) {
        this.numberOfTeachers = numberOfTeachers;
    }

    public void showSchoolInformation() {
        System.out.println("Scoala " + this.name + " din orasul " + this.address
                + " are o capacitate maxima de " + this.numberOfStudents + "cursanti.\nNumarul de profesori din aceasta scoala este de  "
                + this.numberOfTeachers
                + " cu un numar de " + this.numberOfRooms + " clase." +
                "\nRecent scoala a primit o finantare si fatada exterioara a fost varuita in " + this.schoolColor);
    }

    public void defineCourses() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter loop value");
        int t = s.nextInt(); // read number of element
        s.nextLine(); // consume new line
        String str[] = new String[t];
        for (int i = 0; i < t; i++) {
            str[i] = s.nextLine();
        }
        System.out.println(Arrays.toString(str));
    }

    public void showCourses() {
        for (int i = 0; i < courses.length; i++) {
            System.out.println("Courses are" + courses[i]);

        }


    }
}