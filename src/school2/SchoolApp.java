package school2;

public class SchoolApp {
    public static void main(String[] args) {
        School scoala = new School();
        scoala.setName("Petru Maior");
        scoala.setAddress("Reghin");
        scoala.setNumberOfStudents(250);
        scoala.setNumberOfTeachers(10);
        scoala.setNumberOfRooms(11);
        scoala.setSchoolColor("Green");
        scoala.showSchoolInformation();

        Student elev = new Student();
        elev.readGrades();
        elev.showGrades();


    }

}