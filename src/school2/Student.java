package school2;

import java.util.ArrayList;
import java.util.Scanner;

public class Student {
    private String firstName;
    private String lastName;
    private int age;
    private double height;
    private ArrayList<Integer> grades = new ArrayList<Integer>();
    private char gender;
    private boolean orfan;
    private int maxNumberOfGrades = 5;

    public Student() {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti prenumele elevului");
        this.firstName = in.nextLine();
        System.out.println("Introduceti numele elevului");
        this.lastName = in.nextLine();
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isOrfan() {
        return orfan;
    }

    public void setOrfan(boolean orfan) {
        this.orfan = orfan;
    }

    public void readGrades() {
        int grade;
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nIntroduce the grades for student " + getFirstName() + " " + getLastName());
        for (int i = 0; i < maxNumberOfGrades; i++) {
            grade = scanner.nextInt();
            grades.add(grade);

        }

    }

    public void showGrades() {
        System.out.println("The student's grades are ");
        for (int i = 0; i < maxNumberOfGrades; i++) {
            System.out.print(grades.get(i) + " ");
        }
    }


}