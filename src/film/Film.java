package film;

import java.util.ArrayList;
import java.util.Scanner;

public class Film {
    private String titleOfFilm;
    private ArrayList<String> actors = new ArrayList();
    private String director;
    private int numberOfActors;
    private String yearProductionFilm;


    public String getTitleOfFilm() {
        return this.titleOfFilm;
    }

    public ArrayList<String> getActors() {
        return this.actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public void setTitleOfFilm(String titleOfFilm) {
        this.titleOfFilm = titleOfFilm;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getNumberOfActors() {
        return this.numberOfActors;
    }

    public void setNumberOfActors(int numberOfActors) {
        this.numberOfActors = numberOfActors;
    }

    public String getYearProductionFilm() {
        return this.yearProductionFilm;
    }

    public void setYearProductionFilm(String yearProductionFilm) {
        this.yearProductionFilm = yearProductionFilm;
    }

    public void detailsFilm() {

        System.out.println("The movie " + this.titleOfFilm + " is directed by " + this.director +
                " and was released in " + this.yearProductionFilm + "."
                + "The are " + +this.numberOfActors + " actors that star in the movie and they are " + this.actors + "!");
    }

    public void defineActors() {
        Scanner name = new Scanner(System.in);
        System.out.println("Number of actors: \n");
        this.numberOfActors = name.nextInt();
        for (int i = 0; i < this.numberOfActors; i++) {
            Scanner s = new Scanner(System.in);
            System.out.println("Scrieti nume actori");
            String actor = s.nextLine();
            this.actors.add(actor);

        }

    }

    public void showActors() {

        System.out.print("The stars of the movie are ");
        for (int i = 0; i < this.actors.toArray().length; ++i) {
            System.out.print(actors.get(i) + " ");
        }

    }
}
