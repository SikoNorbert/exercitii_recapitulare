package school;

import java.util.Date;

public class Profesor {
    private String firstName;
    private String lastName;
    private int age;
    private String object;
    private String birthDate;

    public Profesor() {
    }

    public String getfirstName() {
        return this.firstName;
    }

    public void setfirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getlastName() {
        return this.lastName;
    }

    public void setlastName(String name) {
        this.lastName = this.lastName;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getObject() {
        return this.object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void informationAboutProfesor() {
        System.out.println(this.firstName + this.lastName + this.age + this.birthDate);
    }
    // indicii :
    // ce materii predau? (metoda)
    // in catalog sa scrie numele elevilor si notele lor? (metoda)
    // care sunt rutinele zilei ale profesorilor? (metoda)
    // ce cerinte au de la elevi la materii de pregatire a examenului de bacalaureat? (metoda)
}