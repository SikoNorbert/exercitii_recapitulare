package school;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Scoala {
    private String name;
    private String address;
    private int numberOfStudents;
    private int numberOfRooms;
    private String schoolColor;
    private int numberOfTeachers;
    private String[][] orar = new String[7][1];
    private ArrayList<String> courses = new ArrayList();
    private String[] telefoane = new String[6];
    private String[] testScapture = new String[6];
    private String[] simulationBacheelor = new String[5];

    public ArrayList<String> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<String> courses) {
        this.courses = courses;
    }

    public String[] getSimulationBacheelor() {
        return this.simulationBacheelor;
    }

    public void setSimulationBacheelor(String[] simulationBacheelor) {
        this.simulationBacheelor = simulationBacheelor;
    }

    public String[] getTestScapture() {
        return this.testScapture;
    }

    public void setTestScapture(String[] testScapture) {
        this.testScapture = testScapture;
    }

    public String[][] getOrar() {
        return this.orar;
    }

    public void setOrar(String[][] orar) {
        this.orar = orar;
    }


    public String[] getTelefoane() {
        return this.telefoane;
    }

    public void setTelefoane(String[] telefoane) {
        this.telefoane = telefoane;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfStudents() {
        return this.numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public int getNumberOfRooms() {
        return this.numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getSchoolColor() {
        return this.schoolColor;
    }

    public void setSchoolColor(String schoolColor) {
        this.schoolColor = schoolColor;
    }

    public int getNumberOfTeachers() {
        return this.numberOfTeachers;
    }

    public void setNumberOfTeachers(int numberOfTeachers) {
        this.numberOfTeachers = numberOfTeachers;
    }

    public void showSchoolInformation() {
        System.out.println(this.name + this.address + this.numberOfStudents + this.numberOfTeachers + this.numberOfRooms + this.schoolColor);
    }

    public void defineCourses() {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter loop value");
        int t = s.nextInt();
        s.nextLine();
        String[] str = new String[t];

        for (int i = 0; i < t; ++i) {
            str[i] = s.nextLine();
        }

        System.out.println(Arrays.toString(str));
    }

    public void showCourses() {
        Scanner in = new Scanner(System.in);
        if(!courses.isEmpty())
            courses.get(0);
        int i;
        int j = 0;
        for ( i = 0; i <= courses.toArray().length; i++) {
                System.out.println("\nCourses are: ");
                courses.toArray()[i]= in.nextLine();


            System.out.println("\nThe courses are: " + courses.toArray()[i]);
                for(j = 0; j <= courses.toArray().length; j++) {
                    courses.toArray()[j] = courses.get(i);
                }
            }
        System.out.println("All the courses are: " + courses.get(i));
        }

    public void copyWithTelephoneOnTests() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in), 1);
        System.out.println("What telephones do you have, dear students: \n");

        for (int i = 0; i <= this.telefoane.length; ++i) {
            for (int j = 0; j <= this.testScapture.length; ++j) {
                System.out.println("False or True: \n");

                try {
                    this.telefoane[i] = in.readLine();
                    this.testScapture[j] = in.readLine();
                } catch (Exception var5) {
                    System.out.println("\nERROR! " + var5.getMessage());
                }

                if (this.telefoane[i] != this.testScapture[j]) {
                    System.out.println("You have copied at my courses. You are penalized! \n");
                } else if (this.telefoane[i] == this.testScapture[j]) {
                    System.out.println("You are generous. You have accomplished my courses without copying! \n");
                }
            }
        }
 // indicii :
        // cati programatori sunt in scoala si ce note au obtinut la programare? (metoda)

    }
}