package school;

import java.util.ArrayList;
import java.util.Scanner;

public class Elev {
    private String firstName;
    private String name;
    private int age;
    private double height;
    private ArrayList<Integer> grades = new ArrayList();
    private char gender;
    private boolean orfan;
    private int maxGrades = 5;


    public int getMaxGrades() {
        return this.maxGrades;
    }

    public void setMaxGrades(int maxGrades) {
        this.maxGrades = maxGrades;
    }

    public ArrayList<Integer> getGrades() {
        return this.grades;
    }

    public void setGrades(ArrayList<Integer> grades) {
        this.grades = grades;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public char getGender() {
        return this.gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isOrfan() {
        return this.orfan;
    }

    public void setOrfan(boolean orfan) {
        this.orfan = orfan;
    }

    public void readGrades() {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < this.maxGrades; ++i) {
            System.out.println("Introduce the grade: ");
            int grade = scanner.nextInt();
            this.grades.add(grade);
        }

    }

    public void showGrades() {
        for (int i = 0; i < this.maxGrades; ++i) {
            int grade = this.grades.get(i);
            System.out.println("Grades are " + grade);
        }

    }
    // indicii:
    // ce vor la scoala altfel? - programe (metoda)
    // cati au ciulit de la ore si cate absente cu rosu au? (metoda)
    // care sunt materiile lor preferate la fumatori din scoala si ce note au obtinut la ele? (metoda)
}


