package school;

public class SchoolApp {
    public SchoolApp() {
    }

    public static void main(String[] args) {
        Scoala scoala = new Scoala();
        scoala.setName("Petru Maior \n");
        scoala.setAddress("Reghin \n");
        scoala.setNumberOfStudents(250);
        scoala.setNumberOfTeachers(10);
        scoala.setNumberOfRooms(11);
        scoala.setSchoolColor("Green");
        scoala.showSchoolInformation();
        scoala.defineCourses();
        scoala.showCourses();
        scoala.copyWithTelephoneOnTests();
        Elev elev = new Elev();
        elev.readGrades();
        elev.showGrades();
        Profesor profesor = new Profesor();
        profesor.setfirstName("Bojte");
        profesor.setlastName("Vilmos");
        profesor.setAge(55);
        profesor.setBirthDate("12.06.1965");
        profesor.informationAboutProfesor();
    }
}